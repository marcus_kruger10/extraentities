﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.POCO;
using System;
namespace eyeshotSampleProj.ViewModels
{
    [POCOViewModel]
    public class NavigationViewModel
    {
        public virtual string Text { get; set; } = "Text";
        protected NavigationViewModel() { }

        public static NavigationViewModel Create()
        {
            return ViewModelSource.Create(() => new NavigationViewModel());
        }

        public void ChangeText()
        {
            Text = "Changed";
        }
    }
}