﻿using DevExpress.XtraEditors;
using eyeshotSampleProj.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace eyeshotSampleProj.Views
{
    public partial class NavigationView : DevExpress.XtraEditors.XtraUserControl
    {
        public NavigationView()
        {
            InitializeComponent();
            if(!mvvmContext1.IsDesignMode)
            {
                InitBindings();
            }
        }

        private void InitBindings()
        {
            var f = mvvmContext1.OfType<NavigationViewModel>();

            f.SetBinding(label1, x => x.Text, x => x.Text);
            f.BindCommand(simpleButton1, x => x.ChangeText);

        }
    }
}
